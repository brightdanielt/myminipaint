package com.example.myminipaint

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
import android.view.WindowInsets
import android.view.WindowInsetsController

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val canvasView = MyCanvasView(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            canvasView.windowInsetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            canvasView.systemUiVisibility = SYSTEM_UI_FLAG_FULLSCREEN
        }
        canvasView.contentDescription = getString(R.string.canvasContentDescription)
        setContentView(canvasView)
    }
}